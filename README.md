# Best backup options for different situations

## Untrusted protected off-site
**Untrusted** (needs client-side encryption) **protected** (client cannot delete files) **off-site** (not root controlled by you)

#### Use
Mainly to back up on-site servers. These on-site servers would also have backups of client computers data.

#### Examples
- [B2](https://www.backblaze.com/b2/cloud-storage.html) (Note: needs an [app key](https://www.backblaze.com/b2/docs/application_keys.html) that only has create and read perms to be "protected")
    - B2 is great for a paid service because it is as cheap as AWS Glacier, but far less restricitve
- another self-hoster's server. This could be accessed via
    - ssh. To be "protected" you would need to use the bindfs solution below
    - s3 compliant servers. note, whatever key they are given for their bucket must be able to only create and read files. deleting or modifying would make this non-"protected"
        - [minio](https://minio.io/)
        - ceph (for the hardcore self-hosters. find your own link)

#### Implementation
- [Perms for allowing creating of files, but not deleting or editing](https://unix.stackexchange.com/a/222758/242906)
    - [Mounting bindfs in fstab](https://bindfs.org/docs/bindfs.1.html) (you'll need to search for `fstab`)
    - If you use this bindfs hack, you will need to use the `--temp-dir` or `--partial-dir` options passed into rsync via the `--rsync-options` option in duplicity so rsync doesn't try to store a partial in the protected dir and mv to put it in the right place. This would fail because of the bindfs protection
- as of this time of reading, [duplicity](https://wiki.archlinux.org/index.php/Duplicity) is best for offsite backups. Make sure to use the `par2+` to have file parity for automatic repair of damaged file if the backend you're using doesn't already have some kind of protection

```log
duplicity --name=b2-backup-bucket \
--encrypt-key <KEY-ID> \
--exclude-device-files --par2-redundancy 20 --progress \
--tempdir /home/user/dup_temp \
/data/ par2+b2://<ID>:<KEY>@<BUCKET>/<FOLDER>
```

## Trusted protected on-site
**Trusted** (no client-side encryption needed) **protected** (client cannot delete files) **on-site** (root controlled by you)

#### Use
To backup servers very often, something that cannot be done as easily over an internet connection, but via a LAN connection would be no problem. Also means a very fast/cheap restore if unaffected.

#### Examples
- A server in-house that is logically seperate from other servers such that it is reasonable that a exploit to other servers wouldn't leak to this one

#### Implementation
Use BTRFS, give a user sudo access to only the `btrfs recieve` command. This would be using the snapshot feature that can use parent snapshots.

## Trusted non-protected on-site

#### Use
Used to back up client computers to on-site servers.

#### Implementation
- BTRFS
    - This is preferred. The only problem I can see is the innability to resume started transfers, which can be very problematic for something mobile like a laptop. My proposed solution is to:
        - creates the typical snapshot with parent client side
        - uses `btrfs send | pigz` client side to cache the snapshot
        - sends the diff to the server with rsync
        - has the server decompress and apply the btrfs snapshot
- [borg](http://borgbackup.readthedocs.io/en/stable/#)
    - easier to handle backups than duplicity. Can actually mount a backup to explore file contents
- [rclone](https://github.com/ncw/rclone): a new very awesome contender. only weakness is potentially no option for SSH transfer
- **[restic](https://github.com/restic/restic)**: seems to be a winner. written in go, it is fast, does snapshot based backups, supports mounting via fuse, and supports many backends (unlike borg only with ssh and rclone not supporting ssh)

## File syncing between clients

#### Use
Used to sync files between client computers, also (optionally) be available by server via a webgui.

#### Implementation
- [Nextcloud](https://nextcloud.com/)
    - more client/server feel
- [Syncthing](https://wiki.archlinux.org/index.php/Syncthing)
    - more p2p feel.